import 'package:asset_inven/model/asset.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

final String dbname = 'nippon.db';

//tb asset inventory
final String dbtable = 'tbassetinven';
final String colId = '_id';
final String colstt = 'stt';
final String collocation = 'location';
final String colboPhanQuanLy = 'boPhanQuanLy';
final String colNguoiSudung = 'nguoisudung';
final String colmaTaiSan = 'maTaiSan';
final String colLoai = 'loai';
final String coltenTaiSan = 'tenTaiSan';
final String colmota = 'moTa';
final String colngayNhap = 'ngayNhap';
final String colghichu = 'ghichu';
final String colScaned = 'scaned';
final String colnotfound = 'notfound';
final String colngayScan = 'ngayscand';
final String colnoiScan = 'noiScand';
final String colsection = 'section';

/// table du // all field in table asset inventory + bellow
final String dbtabledu = 'tbassetinvendu';

/// table thieu // all field in table asset inventory + bellow
final String dbtablethieu = 'tbassetinventhieu';

class MyDatabase {
  Database _database;

  MyDatabase._();

  static final MyDatabase instance = MyDatabase._();

  Future<Database> get getdatabase async {
    if (_database == null) {
      String databasesPath = await getDatabasesPath();
      String dbPath = join(databasesPath, dbname);
      print(dbPath);
      _database = await openDatabase(dbPath, version: 1, onCreate: populateDb);
    }
    return _database;
  }

  void populateDb(Database database, int version) async {
    await database.execute(
      "CREATE TABLE $dbtable ("
      "$colId INTEGER PRIMARY KEY AUTOINCREMENT,"
      "$colstt TEXT,"
      "$colsection TEXT,"
      "$collocation TEXT,"
      "$colboPhanQuanLy TEXT,"
      "$colNguoiSudung TEXT,"
      "$colmaTaiSan TEXT,"
      "$colLoai TEXT,"
      "$coltenTaiSan TEXT,"
      "$colmota TEXT,"
      "$colngayNhap TEXT,"
      "$colghichu TEXT,"
      "$colScaned TEXT,"
      "$colnotfound TEXT,"
      "$colngayScan TEXT,"
      "$colnoiScan TEXT"
      ")",
    );

    await database.execute(
      "CREATE TABLE $dbtabledu ("
      "$colId INTEGER PRIMARY KEY ,"
      "$colstt TEXT,"
      "$colsection TEXT,"
      "$collocation TEXT,"
      "$colboPhanQuanLy TEXT,"
      "$colNguoiSudung TEXT,"
      "$colmaTaiSan TEXT,"
      "$colLoai TEXT,"
      "$coltenTaiSan TEXT,"
      "$colmota TEXT,"
      "$colngayNhap TEXT,"
      "$colghichu TEXT,"
      "$colScaned TEXT,"
      "$colnotfound TEXT,"
      "$colngayScan TEXT,"
      "$colnoiScan TEXT"
      ")",
    );

    await database.execute(
      "CREATE TABLE $dbtablethieu ("
      "$colId INTEGER PRIMARY KEY AUTOINCREMENT,"
      "$colstt TEXT,"
      "$colsection TEXT,"
      "$collocation TEXT,"
      "$colboPhanQuanLy TEXT,"
      "$colNguoiSudung TEXT,"
      "$colmaTaiSan TEXT,"
      "$colLoai TEXT,"
      "$coltenTaiSan TEXT,"
      "$colmota TEXT,"
      "$colngayNhap TEXT,"
      "$colghichu TEXT,"
      "$colScaned TEXT,"
      "$colnotfound TEXT,"
      "$colngayScan TEXT,"
      "$colnoiScan TEXT"
      ")",
    );
  }

  Future<int> insertAsset(Asset asset) async {
    Database db = await instance.getdatabase;
    var result = await db.insert(dbtable, asset.toMap());
    return result;
  }

  Future<int> insertAssetDu(Asset asset) async {
    Database db = await instance.getdatabase;
    var result = await db.insert(dbtabledu, asset.toMap());
    return result;
  }

  Future<int> insertAssetThieu(Asset asset) async {
    Database db = await instance.getdatabase;
    var result = await db.insert(dbtablethieu, asset.toMap());
    return result;
  }

  Future<List<Asset>> getListAssetbyBophan(
      String strBoPhan, bool isScanned) async {
    Database db = await instance.getdatabase;
    List<Map> maps = await db.query(
      dbtable,
      columns: [
        colId,
        colstt,
        collocation,
        colsection,
        colboPhanQuanLy,
        colNguoiSudung,
        colmaTaiSan,
        colLoai,
        coltenTaiSan,
        colmota,
        colngayNhap,
        colghichu,
        colScaned,
        colnotfound,
        colngayScan,
        colnoiScan
      ],
      where: '$colboPhanQuanLy = ? and $colScaned <> ? ',
      whereArgs: [strBoPhan, 'scanned'],
      // where:
      //     '$colboPhanQuanLy = ? and  ${isScanned == true ? colScaned + '<> ? ' : colScaned + '= ?'} ',
      // whereArgs: [strBoPhan, 'scanned'],
    );
    if (maps.length > 0) {
      return maps.map((e) => Asset.fromMap(e)).toList();
    }
    return [];
  }

  Future<List<Asset>> getListAssetbyLocation(
      String strLocation, bool isScanned) async {
    Database db = await instance.getdatabase;
    List<Map> maps = await db.query(
      dbtable,
      columns: [
        colId,
        colstt,
        collocation,
        colsection,
        colboPhanQuanLy,
        colNguoiSudung,
        colmaTaiSan,
        colLoai,
        coltenTaiSan,
        colmota,
        colngayNhap,
        colghichu,
        colScaned,
        colnotfound,
        colngayScan,
        colnoiScan
      ],
      where: '$collocation = ? and $colScaned <> ? ',
      whereArgs: [strLocation, 'scanned'],
    );
    if (maps.length > 0) {
      return maps.map((e) => Asset.fromMap(e)).toList();
    }
    return [];
  }

  Future<List<Asset>> getListAssetAllAsset() async {
    Database db = await instance.getdatabase;
    List<Map> maps = await db.query(
      dbtable,
      columns: [
        colId,
        colstt,
        collocation,
        colsection,
        colboPhanQuanLy,
        colNguoiSudung,
        colmaTaiSan,
        colLoai,
        coltenTaiSan,
        colmota,
        colngayNhap,
        colghichu,
        colScaned,
        colnotfound,
        colngayScan,
        colnoiScan
      ],
      where: '$colScaned = ?',
      whereArgs: ['scanned'],
    );
    if (maps.length > 0) {
      return maps.map((e) => Asset.fromMap(e)).toList();
    }
    return [];
  }

  Future<List<Asset>> getListAssetAllAssetDuByType(String strType) async {
    Database db = await instance.getdatabase;
    List<Map> maps = await db.query(
      dbtabledu,
      columns: [
        colId,
        colstt,
        collocation,
        colsection,
        colboPhanQuanLy,
        colNguoiSudung,
        colmaTaiSan,
        colLoai,
        coltenTaiSan,
        colmota,
        colngayNhap,
        colghichu,
        colScaned,
        colnotfound,
        colngayScan,
        colnoiScan
      ],
      where: '$colLoai = ?',
      whereArgs: [
        strType,
      ],
    );
    if (maps.length > 0) {
      return maps.map((e) => Asset.fromMap(e)).toList();
    }
    return [];
  }

  Future<List<Asset>> getListAssetbyBophanDu() async {
    Database db = await instance.getdatabase;
    List<Map> maps = await db.query(
      dbtabledu,
      columns: [
        colId,
        colstt,
        collocation,
        colsection,
        colboPhanQuanLy,
        colNguoiSudung,
        colmaTaiSan,
        colLoai,
        coltenTaiSan,
        colmota,
        colngayNhap,
        colghichu,
        colScaned,
        colnotfound,
        colngayScan,
        colnoiScan
      ],
    );
    if (maps.length > 0) {
      return maps.map((e) => Asset.fromMap(e)).toList();
    }
    return [];
  }

  Future<List<Asset>> getListAssetMissing() async {
    Database db = await instance.getdatabase;
    List<Map> maps = await db.query(
      dbtablethieu,
      columns: [
        colId,
        colstt,
        collocation,
        colsection,
        colboPhanQuanLy,
        colNguoiSudung,
        colmaTaiSan,
        colLoai,
        coltenTaiSan,
        colmota,
        colngayNhap,
        colghichu,
        colScaned,
        colnotfound,
        colngayScan,
        colnoiScan
      ],
    );
    if (maps.length > 0) {
      return maps.map((e) => Asset.fromMap(e)).toList();
    }
    return [];
  }

  Future<List<String>> getListBoPhan() async {
    Database db = await instance.getdatabase;
    List<Map<String, dynamic>> maps = await db.query(
      dbtable,
      columns: [
        colId,
        colstt,
        colsection,
        collocation,
        colboPhanQuanLy,
        colNguoiSudung,
        colmaTaiSan,
        colLoai,
        coltenTaiSan,
        colmota,
        colngayNhap,
        colghichu,
        colScaned,
        colnotfound,
        colngayScan,
        colnoiScan
      ],
      groupBy: '$colsection',
    );
    if (maps.length > 0) {
      return maps.map<String>((e) => e[colsection]).toList();
    }
    return [];
  }

  Future<List<String>> getListLocation() async {
    Database db = await instance.getdatabase;
    List<Map<String, dynamic>> maps = await db.query(
      dbtable,
      columns: [
        colId,
        colstt,
        collocation,
        colsection,
        colboPhanQuanLy,
        colNguoiSudung,
        colmaTaiSan,
        colLoai,
        coltenTaiSan,
        colmota,
        colngayNhap,
        colghichu,
        colScaned,
        colnotfound,
        colngayScan,
        colnoiScan
      ],
      groupBy: '$collocation',
    );
    if (maps.length > 0) {
      List<String> list = maps.map<String>((e) => e[collocation]).toList();
      return list;
    }
    return [];
  }

  Future<Asset> getAsset(String strMaTaiSan) async {
    Database db = await instance.getdatabase;
    List<Map> maps = await db.query(
      dbtable,
      columns: [
        colId,
        colstt,
        colsection,
        collocation,
        colboPhanQuanLy,
        colNguoiSudung,
        colmaTaiSan,
        colLoai,
        coltenTaiSan,
        colmota,
        colngayNhap,
        colghichu,
        colScaned,
        colnotfound,
        colngayScan,
        colnoiScan
      ],
      where: '$colmaTaiSan = ?',
      whereArgs: [strMaTaiSan],
    );
    if (maps.length > 0) {
      return Asset.fromMap(maps.first);
    }

    return null;
  }

  Future<String> getRemain(String strLocation) async {
    Database db = await instance.getdatabase;
    List<Map> maps = await db.query(
      dbtable,
      columns: ['Count(*) as num'],
      where: '$collocation = ?  and $colScaned <> ? ',
      whereArgs: [strLocation, 'scanned'],
      groupBy: ' $collocation',
    );
    if (maps.length > 0) {
      return maps.first['num'].toString();
    }

    return null;
  }

  Future<Asset> getAssetDu(String strMaTaiSan) async {
    Database db = await instance.getdatabase;
    List<Map> maps = await db.query(
      dbtabledu,
      columns: [
        colId,
        colstt,
        colsection,
        collocation,
        colboPhanQuanLy,
        colNguoiSudung,
        colmaTaiSan,
        colLoai,
        coltenTaiSan,
        colmota,
        colngayNhap,
        colghichu,
        colScaned,
        colnotfound,
        colngayScan,
        colnoiScan
      ],
      where: '$colmaTaiSan = ?',
      whereArgs: [strMaTaiSan],
    );
    if (maps.length > 0) {
      return Asset.fromMap(maps.first);
    }

    return null;
  }

  Future<Asset> getAssetthieu(String strMaTaiSan) async {
    Database db = await instance.getdatabase;
    List<Map> maps = await db.query(
      dbtablethieu,
      columns: [
        colId,
        colstt,
        colsection,
        collocation,
        colboPhanQuanLy,
        colNguoiSudung,
        colmaTaiSan,
        colLoai,
        coltenTaiSan,
        colmota,
        colngayNhap,
        colghichu,
        colScaned,
        colnotfound,
        colngayScan,
        colnoiScan
      ],
      where: '$colmaTaiSan = ?',
      whereArgs: [strMaTaiSan],
    );
    if (maps.length > 0) {
      return Asset.fromMap(maps.first);
    }

    return null;
  }

  Future<int> updateAsset(Asset asset) async {
    Database db = await instance.getdatabase;
    return await db.update(
      dbtable,
      asset.toMap(),
      where: '$colId = ?',
      whereArgs: [asset.id],
    );
  }

  Future close() async => instance.getdatabase.then((value) => value.close());
}
