import 'dart:io';
import 'dart:isolate';
import 'dart:typed_data';
import 'package:flutter/services.dart' show ByteData, rootBundle;
import 'package:asset_inven/database/database.dart';
import 'package:asset_inven/model/asset.dart';
import 'package:excel/excel.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'package:path_provider/path_provider.dart';

Future<int> readFileExcel({@required String dir}) async {
  if (dir != '') {
    Uint8List bytes = File(dir).readAsBytesSync();

    Excel excel = await compute(_decodeButyes, bytes);
    Map<String, Sheet> table = excel.tables;
    Sheet sheet = table["CCDC"];
    Sheet sheet2 = table["TSCĐ"];
    MyDatabase myDatabase = MyDatabase.instance;

    for (int row = 3; row < sheet.maxRows; row++) {
      List<Data> dataRow = sheet.row(row);

      await myDatabase.insertAsset(
        Asset(
          stt: getCellValue(dataRow[0]),
          location: getCellValue(dataRow[1]),
          section: getCellValue(dataRow[2]),
          boPhanQuanLy: getCellValue(dataRow[2]),
          nguoisudung: getCellValue(dataRow[3]),
          maTaiSan: getCellValue(dataRow[4]),
          loai: getCellValue(dataRow[5]),
          tenTaiSan: getCellValue(dataRow[6]),
          moTa: getCellValue(dataRow[7]),
          ngayNhap: getCellValue(dataRow[8]),
          ghichu: getCellValue(dataRow[9]),
          scanned: '',
          notfound: '',
          ngayscand: '',
          noiScand: '',
        ),
      );
    }
    for (int row = 3; row < sheet2.maxRows; row++) {
      List<Data> dataRow = sheet2.row(row);
      // Data data = sheet2
      //     .cell(CellIndex.indexByColumnRow(rowIndex: row, columnIndex: 8));
      // final df = new DateFormat('dd/MM/yyyy');
      //  String s = df.format(new DateTime.fromMillisecondsSinceEpoch(data.value)

      await myDatabase.insertAsset(
        Asset(
          stt: getCellValue(dataRow[0]),
          location: getCellValue(dataRow[1]),
          section: getCellValue(dataRow[2]),
          boPhanQuanLy: getCellValue(dataRow[2]),
          nguoisudung: getCellValue(dataRow[3]),
          maTaiSan: getCellValue(dataRow[4]),
          loai: getCellValue(dataRow[5]),
          tenTaiSan: getCellValue(dataRow[6]),
          moTa: getCellValue(dataRow[7]),
          ngayNhap: getCellValue(dataRow[8]),
          ghichu: getCellValue(dataRow[9]),
          scanned: '',
          notfound: '',
          ngayscand: '',
          noiScand: '',
        ),
      );
    }
    return Future.value(1);
  }
  return Future.value(1);
}

Future showMesageDialog(context, String message) {
  return showDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text('Info'),
          content: Text(message),
          actions: [
            FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('OK'))
          ],
        );
      });
}

Future<String> getDir() async {
  FilePickerResult result = await FilePicker.platform.pickFiles();

  if (result != null) {
    File file = File(result.files.single.path);
    return file.path;
  } else {
    // User canceled the picker
  }
  return '';
}

SmtpServer outlookmail(String username, String password) =>
    SmtpServer('smtp-mail.outlook.com', username: username, password: password);

String getCellValue(Data data) {
  try {
    if (data.value.toString() == 'null') {
      return '';
    }
    return data.value.toString().trim();
  } catch (e) {
    return '';
  }
}

Future createOtherExcelFile(List<Asset> dataSource, bool isRemain) async {
  try {
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;

    ByteData data = await rootBundle.load("assets/book1.xlsx");
    var bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    var excel = await compute(_decodeButyes, bytes);
    Future.wait([handOrtherData(dataSource, excel)]).then((value) async {
      List<int> encode = value[0];
      String pathFile =
          isRemain ? tempPath + "/data_du.xlsx" : tempPath + "/data_thieu.xlsx";

      File(pathFile)
        ..createSync(recursive: true)
        ..writeAsBytesSync(encode);
      await _sendEmail([FileAttachment(File(pathFile))]);

      File(pathFile).delete();
    }).catchError((e) {
      return Future.error(e);
    });
  } catch (e) {
    return Future.error(e);
  }
}

Future<List<int>> handOrtherData(List<Asset> dataSource, Excel excel) async {
  Sheet sheet1 = excel['Sheet1'];
  _handleOrtherDataWrite(dataSource, sheet1);
  var encode = await excel.encode();

  return encode;
}

_handleOrtherDataWrite(List<Asset> dataSource, Sheet sheet1) {
  int index = 2;
  int no = 1;
  dataSource.forEach((element) {
    sheet1.cell(CellIndex.indexByString('A$index'))..value = no++;
    sheet1.cell(CellIndex.indexByString('B$index'))..value = element.location;
    sheet1.cell(CellIndex.indexByString('C$index'))
      ..value = element.boPhanQuanLy;
    sheet1.cell(CellIndex.indexByString('D$index'))
      ..value = element.nguoisudung;
    sheet1.cell(CellIndex.indexByString('E$index'))..value = element.maTaiSan;
    sheet1.cell(CellIndex.indexByString('F$index'))..value = element.loai;
    sheet1.cell(CellIndex.indexByString('G$index'))..value = element.tenTaiSan;
    sheet1.cell(CellIndex.indexByString('H$index'))..value = element.moTa;
    sheet1.cell(CellIndex.indexByString('I$index'))..value = element.ngayNhap;
    sheet1.cell(CellIndex.indexByString('J$index'))..value = element.ghichu;
    sheet1.cell(CellIndex.indexByString('K$index'))..value = element.ngayscand;
    sheet1.cell(CellIndex.indexByString('L$index'))..value = element.noiScand;
    index++;
  });
}

Future sendReceive(SendPort send, message) {
  ReceivePort receivePort = ReceivePort();
  send.send([message, receivePort.sendPort]);
  return receivePort.first;
}

Future createExcelFile(List<Asset> dataSource) async {
  try {
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    ByteData data = await rootBundle.load("assets/file.xlsx");
    var bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    var excel = await compute(_decodeButyes, bytes);

    Future.wait([handleData(excel, dataSource)]).then((value) async {
      List<int> bytes = value[0];
      String pathFile = tempPath + "/form1.xlsx";
      File(pathFile)
        ..createSync(recursive: true)
        ..writeAsBytesSync(bytes);

      await _sendEmail([FileAttachment(File(pathFile))]);
      File(pathFile).delete();
    }).catchError((e) {
      return Future.error(e);
    });
  } catch (e) {
    return Future.error(e);
  }
}

Future<List<int>> handleData(Excel excel, List<Asset> dataSource) async {
  List<Asset> listFixed =
      dataSource.where((element) => element.loai == 'Fix asset').toList();
  List<Asset> listEquipment =
      dataSource.where((element) => element.loai == 'Equipment').toList();

  Sheet sheetCcDc = excel['CCDC'];
  Sheet sheetTsCd = excel['TSCĐ'];

  _fetchData(listFixed, sheetTsCd);
  _fetchData(listEquipment, sheetCcDc);

  var value = await excel.encode();

  return value;
}

Future<SendReport> _sendEmail(List<Attachment> listfilePath) async {
  final smtpServer = outlookmail('ngotluan@nittsu.com.vn', "Tlp281100");
  var message = Message()
    ..from = Address(
      "ngotluan@nittsu.com.vn",
    )
    ..recipients.add('nguyenhtdung@nittsu.com.vn')
    ..ccRecipients.add("sgnit@nittsu.com.vn")
    ..subject = "Kiem ke tai san Nippon"
    ..text = "Dear All\n No reply, thank you\n File attached below!!!\n"
    ..attachments = listfilePath;
  try {
    final sendReport = await send(message, smtpServer);
    return sendReport;
  } on MailerException catch (e) {
    return Future.error(e);
  }
}

void _fetchData(List<Asset> lstData, Sheet sheet) {
  int index = 13;
  int no = 1;

  for (var i = 0; i < lstData.length; i++) {
    sheet.cell(CellIndex.indexByString('A$index'))..value = no++;
    sheet.cell(CellIndex.indexByString('B$index'))..value = lstData[i].location;
    sheet.cell(CellIndex.indexByString('C$index'))
      ..value = lstData[i].boPhanQuanLy;
    sheet.cell(CellIndex.indexByString('D$index'))
      ..value = lstData[i].nguoisudung;
    sheet.cell(CellIndex.indexByString('E$index'))..value = lstData[i].maTaiSan;
    sheet.cell(CellIndex.indexByString('F$index'))..value = lstData[i].loai;
    sheet.cell(CellIndex.indexByString('G$index'))
      ..value = lstData[i].tenTaiSan;
    sheet.cell(CellIndex.indexByString('H$index'))..value = lstData[i].moTa;
    sheet.cell(CellIndex.indexByString('I$index'))..value = lstData[i].ngayNhap;
    sheet.cell(CellIndex.indexByString('J$index'))..value = lstData[i].ghichu;
    index++;

    if (i == lstData.length - 1) {
      index++;
      sheet.cell(CellIndex.indexByString('B$index'))..value = "Bộ Phận Quản Lý";

      sheet.merge(CellIndex.indexByString('C$index'),
          CellIndex.indexByString('G$index'),
          customValue: "Tổ kiểm kê");

      sheet.cell(CellIndex.indexByString('I$index'))..value = "Ban giám đốc";

      sheet.merge(CellIndex.indexByString('C${index += 4}'),
          CellIndex.indexByString('G${index += 4}'),
          customValue:
              "Trần Thanh Tùng \t\t Nguyễn Quang Quý \t\t Nguyễn Hòa Trung Dũng \t\t Nguyễn Thị Trang \t\t Đặng Thị Như Nguyệt");
      sheet.cell(CellIndex.indexByString('I$index'))..value = "Tôn Thất Hưng";
    }
  }
}

Excel _decodeButyes(var bytes) {
  return Excel.decodeBytes(bytes);
}

final GlobalKey<FormFieldState<String>> globalKey =
    GlobalKey<FormFieldState<String>>();
String _date = DateTime.now().toString();
DateTime _dateParse = DateTime.parse(_date);
String myCurrentDate =
    '${_dateParse.day}/${_dateParse.month}/${_dateParse.year} ${_dateParse.hour}:${_dateParse.minute}';
