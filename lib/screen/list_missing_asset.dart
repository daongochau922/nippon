import 'package:asset_inven/conponent/text_description.dart';
import 'package:asset_inven/database/database.dart';
import 'package:asset_inven/model/asset.dart';
import 'package:flutter/material.dart';

class ListMissingAssetScreen extends StatelessWidget {
  static String routeName = '/listmissngasset';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List missing asset'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FutureBuilder<List<Asset>>(
          future: MyDatabase.instance.getListAssetMissing(),
          builder: (context, snapshot) {
            List<Asset> list = [];
            list = snapshot.data;
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            } else {
              return ListView.builder(
                itemCount: list.length,
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.only(bottom: 5),
                    decoration: BoxDecoration(
                      color: Colors.blue.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextDescription(
                          text1: 'Code ',
                          text2: list[index].maTaiSan,
                        ),
                        TextDescription(
                          text1: 'Name ',
                          text2: list[index].tenTaiSan,
                        ),
                        TextDescription(
                          text1: 'Section ',
                          text2: list[index].boPhanQuanLy,
                        ),
                        TextDescription(
                          text1: 'location ',
                          text2: list[index].boPhanQuanLy,
                        ),
                        TextDescription(
                          text1: 'PIC ',
                          text2: list[index].nguoisudung,
                        ),
                        TextDescription(
                          text1: 'Description ',
                          text2: list[index].moTa,
                        ),
                        TextDescription(
                          text1: 'Date scanned ',
                          text2: list[index].ngayscand,
                        ),
                      ],
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}
