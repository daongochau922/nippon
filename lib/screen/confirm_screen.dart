import 'package:asset_inven/conponent/text_description.dart';
import 'package:asset_inven/database/database.dart';
import 'package:asset_inven/model/asset.dart';
import 'package:flutter/material.dart';

class ConfirmScreen extends StatefulWidget {
  static String routeName = '/confirmscreen';

  @override
  _ConfirmScreenState createState() => _ConfirmScreenState();
}

class _ConfirmScreenState extends State<ConfirmScreen> {
  @override
  Widget build(BuildContext context) {
    final ConfirmScreenArgument args =
        ModalRoute.of(context).settings.arguments;
    final String location = args.location;
    return Scaffold(
      appBar: AppBar(
        title: Text('Confirm'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextDescription(
              text1: 'Location ',
              text2: location,
            ),
            FutureBuilder<List<Asset>>(
              future:
                  MyDatabase.instance.getListAssetbyLocation(location, false),
              builder: (context, snapshot) {
                List<Asset> listbophan = [];
                listbophan = snapshot.data;
                if (!snapshot.hasData) {
                  return Expanded(
                      child: Center(child: CircularProgressIndicator()));
                } else if (listbophan.length == 0) {
                  return Expanded(
                    child: Center(
                      child: Text(
                        'Completed',
                        style: TextStyle(color: Colors.red, fontSize: 40),
                      ),
                    ),
                  );
                } else {
                  return Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        TextDescription(
                          text1: 'Remain ',
                          text2: listbophan.length.toString(),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Expanded(
                          child: ListView.builder(
                            itemCount: listbophan.length,
                            itemBuilder: (context, index) => Container(
                              padding: EdgeInsets.all(10),
                              margin: EdgeInsets.only(bottom: 10),
                              decoration: BoxDecoration(
                                color: Colors.blue.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  TextDescription(
                                    text1: 'Code ',
                                    text2: listbophan[index].maTaiSan,
                                  ),
                                  TextDescription(
                                    text1: 'Name ',
                                    text2: listbophan[index].tenTaiSan,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}

class ConfirmScreenArgument {
  final String location;

  ConfirmScreenArgument({this.location});
}
