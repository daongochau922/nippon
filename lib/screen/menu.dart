import 'dart:async';

import 'package:asset_inven/Bloc/load_bophan_stream.dart';
import 'package:asset_inven/Bloc/load_location_stream.dart';
import 'package:asset_inven/conponent/cricle_dialog.dart';
import 'package:asset_inven/conponent/text_description.dart';
import 'package:asset_inven/database/database.dart';
import 'package:asset_inven/helper/connectivity.dart';
import 'package:asset_inven/model/asset.dart';

import 'package:asset_inven/screen/add_missing_asset_screen.dart';
import 'package:asset_inven/screen/confirm_screen.dart';
import 'package:asset_inven/screen/list_missing_asset.dart';
import 'package:asset_inven/screen/list_resedual_screen.dart';

import 'package:flutter/foundation.dart';

import 'package:asset_inven/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:toast/toast.dart';

import '../conponent/cricle_dialog.dart';

import '../database/database.dart';

// ignore: must_be_immutable
class MenuScreen extends StatelessWidget {
  static String routeName = '/menuscreen';
  BlocLoadBoPhan blocload = BlocLoadBoPhan();
  ConnectivityNetwork connectionStatus = ConnectivityNetwork.getInstance();
  BlocLoadLocation blocLOCload = BlocLoadLocation();

  @override
  Widget build(BuildContext context) {
    List<String> listMenu = [
      'List residual asset',
      'List missing asset',
      'Export Data',
      'Export Remain Data',
      'Export Missing Data',
      'Upload file excel'
    ];
    FocusScope.of(context).requestFocus(FocusNode());
    return Scaffold(
      appBar: AppBar(
        title: Text('Asset Inventory'),
        actions: [
          PopupMenuButton<String>(
            child: Icon(
              Icons.more_vert,
            ),
            onSelected: (value) async {
              switch (value) {
                case 'List residual asset':
                  Navigator.pushNamed(context, ListResidualScreen.routeName);
                  break;
                case 'List missing asset':
                  Navigator.pushNamed(
                      context, ListMissingAssetScreen.routeName);
                  break;
                case 'Upload file excel':
                  // showDialogUploadFileExcel(context);
                  uploadFileExcel(context, blocload, blocLOCload);
                  break;
                case 'Export Data':
                  exportFile(context);
                  break;
                case 'Export Remain Data':
                  exportOthderData(context, true);
                  break;
                case 'Export Missing Data':
                  exportOthderData(context, false);
                  break;
                default:
              }
            },
            itemBuilder: (context) => listMenu
                .map(
                  (choice) => PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  ),
                )
                .toList(),
          ),
        ],
      ),
      body: Body(
        bloc: blocload,
        blocLOC: blocLOCload,
      ),
    );
  }

  void exportFile(context) async {
    if (await connectionStatus.checkConnection()) {
      LoadingDialog.showLoadingDialog(context);

      List<Asset> listDataSource =
          await MyDatabase.instance.getListAssetAllAsset();

      await createExcelFile(listDataSource).whenComplete(() {
        LoadingDialog.hideLoadingDialog(context);
        showMesageDialog(context, "Email sent");
      }).catchError((e) {
        LoadingDialog.hideLoadingDialog(context);
        showMesageDialog(context, e.toString());
      });
    } else {
      showMesageDialog(
          context, "No Connection Internet, Please check connection");
    }
  }

  Future showDialogUploadFileExcel(BuildContext context) {
    return showDialog(
      context: context,
      child: AlertDialog(
        title: Text('Warning!!!!'),
        content: Text(
          'If you uploaded file excel.\nPlease not upload it again.',
        ),
        actions: [
          FlatButton(
            onPressed: () {
              uploadFileExcel(context, blocload, blocLOCload);
              Navigator.pop(context);
            },
            child: Text('OK'),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text('Cancel'),
          ),
        ],
      ),
    );
  }

  uploadFileExcel(BuildContext context, BlocLoadBoPhan bloc,
      BlocLoadLocation blocLOC) async {
    LoadingDialog.showLoadingDialog(context);

    String dir = await getDir();
    if (dir == '') {
      Toast.show('Please select file', context,
          gravity: 1, duration: Toast.LENGTH_LONG);
    } else {
      await readFileExcel(dir: dir);
    }
    blocload.getData();
    blocLOCload.getData();

    LoadingDialog.hideLoadingDialog(context);
    Toast.show('Upload data complete', context,
        gravity: 1, duration: Toast.LENGTH_LONG);
  }

  void exportOthderData(BuildContext context, bool isRemain) async {
    if (await connectionStatus.checkConnection()) {
      LoadingDialog.showLoadingDialog(context);

      List<Asset> listDataSourceDu =
          await MyDatabase.instance.getListAssetbyBophanDu();

      List<Asset> listDataSourceMissing =
          await MyDatabase.instance.getListAssetMissing();
      if (isRemain) {
        createOtherExcelFile(listDataSourceDu, true).whenComplete(() {
          LoadingDialog.hideLoadingDialog(context);
          showMesageDialog(context, "Email sent");
        }).catchError((e) => LoadingDialog.hideLoadingDialog(context));
      } else {
        createOtherExcelFile(listDataSourceMissing, false).whenComplete(() {
          LoadingDialog.hideLoadingDialog(context);
          showMesageDialog(context, "Email sent");
        }).catchError((e) => LoadingDialog.hideLoadingDialog(context));
      }
    } else {
      showMesageDialog(
          context, "No Connection Internet, Please check connection");
    }
  }
}

class Body extends StatefulWidget {
  const Body({
    Key key,
    this.bloc,
    this.blocLOC,
  }) : super(key: key);
  final BlocLoadBoPhan bloc;
  final BlocLoadLocation blocLOC;

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  MyDatabase myDatabase = MyDatabase.instance;
  List<String> listBoPhan = [];
  List<String> listLocation = [];
  String valueLocation = '';
  String valueBophan = '';
  FocusNode focusScan;
  FocusNode focusPIC;
  TextEditingController scanController;
  TextEditingController picColtroller;
  Asset asset;
  bool assetDu = false;
  String remain = '';
  @override
  void initState() {
    widget.bloc.getData();
    widget.blocLOC.getData();
    focusPIC = FocusNode();
    focusScan = FocusNode();
    scanController = new TextEditingController();
    picColtroller = new TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    widget.bloc.dispose();
    widget.blocLOC.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text('Location:  '),
                StreamBuilder<Future<List<String>>>(
                  stream: widget.blocLOC.loadLocationStream,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Center(child: CircularProgressIndicator());
                    } else {
                      return FutureBuilder<List<String>>(
                        future: snapshot.data,
                        builder: (context, snapshot) {
                          listLocation = snapshot.data;
                          if (listLocation != null && listLocation.length > 0) {
                            if (valueLocation == '') {
                              valueLocation = listLocation[0];
                            }
                          }
                          if (!snapshot.hasData) {
                            return CircularProgressIndicator();
                          } else {
                            return DropdownButton<String>(
                              value: valueLocation,
                              hint: Text('Please select'),
                              items: listLocation.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (String value) {
                                setState(() {
                                  valueLocation = value;
                                });
                              },
                            );
                          }
                        },
                      );
                    }
                  },
                ),
              ],
            ),
            TextField(
              controller: scanController,
              focusNode: focusScan,
              decoration: InputDecoration(
                hintText: 'Scan',
              ),
              onSubmitted: (value) {
                scan(value);
              },
            ),
            SizedBox(
              height: 10,
            ),
            asset != null
                ? TextDescription(text1: 'Name', text2: asset.tenTaiSan)
                : Text(''),
            asset != null
                ? TextDescription(text1: 'Location', text2: asset.location)
                : Text(''),
            // asset != null
            //     ? TextDescription(text1: 'Section', text2: asset.boPhanQuanLy)
            //     : Text(''),
            asset != null
                ? TextDescription(text1: 'Type', text2: asset.loai)
                : Text(''),
            asset != null
                ? TextDescription(text1: 'Description', text2: asset.moTa)
                : Text(''),
            asset != null
                ? TextDescription(text1: 'Date scanned', text2: asset.ngayscand)
                : Text(''),
            asset != null
                ? TextDescription(
                    text1: 'Section Scanned', text2: asset.noiScand)
                : Text(''),
            assetDu == true
                ? Text(
                    'This item not in this Location',
                    style: TextStyle(backgroundColor: Colors.yellowAccent),
                  )
                : Text(''),
            Row(
              children: [
                Text('Section:  '),
                StreamBuilder<Future<List<String>>>(
                  stream: widget.bloc.loadBophanStream,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Center(child: CircularProgressIndicator());
                    } else {
                      return FutureBuilder<List<String>>(
                        future: snapshot.data,
                        builder: (context, snapshot) {
                          listBoPhan = snapshot.data;
                          if (listBoPhan != null && listBoPhan.length > 0) {
                            if (valueBophan == '') {
                              valueBophan = listBoPhan[0];
                            }
                          }
                          if (!snapshot.hasData) {
                            return CircularProgressIndicator();
                          } else {
                            return DropdownButton<String>(
                              value: valueBophan,
                              hint: Text('Please select'),
                              items: listBoPhan.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (String value) {
                                setState(() {
                                  valueBophan = value;
                                });
                              },
                            );
                          }
                        },
                      );
                    }
                  },
                ),
                // DropdownButton<String>(
                //   value: valueBophan,
                //   hint: Text('Please select'),
                //   items:
                //       listBoPhan.map<DropdownMenuItem<String>>((String value) {
                //     return DropdownMenuItem<String>(
                //       value: value,
                //       child: Text(value),
                //     );
                //   }).toList(),
                //   onChanged: (String value) {
                //     setState(() {
                //       valueBophan = value;
                //     });
                //   },
                // ),
              ],
            ),
            Row(
              children: [
                Text('PIC:'),
                Expanded(
                  child: TextField(
                    focusNode: focusPIC,
                    controller: picColtroller,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(1),
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                FlatButton(
                  color: Colors.blueAccent.withOpacity(0.9),
                  onPressed: () async {
                    if (asset != null) {
                      asset.noiScand = valueLocation;
                      asset.ngayscand = myCurrentDate;
                      asset.nguoisudung = picColtroller.text;
                      asset.boPhanQuanLy = valueBophan;
                      asset.scanned = 'scanned';
                      await myDatabase.updateAsset(asset);

                      remain =
                          await MyDatabase.instance.getRemain(valueLocation);
                      setState(() {});

                      Toast.show('Register complete', context,
                          gravity: 1, duration: Toast.LENGTH_LONG);
                    } else {
                      Toast.show('Please add to\nmissing asset List', context,
                          gravity: 1, duration: Toast.LENGTH_LONG);
                    }
                    focusScan.requestFocus();
                    scanController.selection = TextSelection(
                      baseOffset: 0,
                      extentOffset: scanController.text.length,
                    );
                  },
                  child: Text(
                    'Register',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                FlatButton(
                    color: Colors.blueAccent.withOpacity(0.9),
                    onPressed: () {
                      Navigator.pushNamed(context, ConfirmScreen.routeName,
                          arguments: new ConfirmScreenArgument(
                              location: valueLocation));
                    },
                    child: Column(
                      children: [
                        Text(
                          'Remain',
                          style: TextStyle(color: Colors.white),
                        ),
                        remain != ''
                            ? Container(
                                padding: EdgeInsets.fromLTRB(5, 1, 5, 1),
                                decoration: BoxDecoration(
                                  color: Colors.red[300],
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                child: Text(
                                  remain.toString(),
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    )),
                assetDu == true
                    ? FlatButton(
                        color: Colors.blueAccent.withOpacity(0.9),
                        onPressed: () async {
                          print(asset.noiScand);
                          if (asset.noiScand != '') {
                            Asset temp = await MyDatabase.instance
                                .getAssetDu(asset.maTaiSan);
                            if (temp == null) {
                              await MyDatabase.instance.insertAssetDu(asset);
                              Toast.show(
                                  'Insert to resedual list\ncompleted', context,
                                  gravity: 1, duration: Toast.LENGTH_LONG);
                            } else {
                              Toast.show("Code already exist in resudual list",
                                  context,
                                  gravity: 1, duration: Toast.LENGTH_LONG);
                            }
                          } else {
                            Toast.show('Please Register before', context,
                                gravity: 1, duration: Toast.LENGTH_LONG);
                          }
                        },
                        child: Text(
                          'Add to resedual\nasset list',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    : Container(),
                asset == null && scanController.text.trim() != ''
                    ? FlatButton(
                        color: Colors.blueAccent.withOpacity(0.9),
                        onPressed: () {
                          Navigator.pushNamed(
                            context,
                            AddMissingAssetScreen.routeName,
                            arguments: AddMssingAssetScreenArgument(
                                code: scanController.text,
                                bophan: valueBophan,
                                location: valueLocation,
                                pic: picColtroller.text),
                          );
                        },
                        child: Text(
                          'Add to missing \nasset list',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  scan(String value) {
    MyDatabase myDatabase = MyDatabase.instance;
    myDatabase.getAsset(value).then((value) async {
      remain = await MyDatabase.instance.getRemain(valueLocation);
      setState(() {
        asset = value;
        assetDu = false;
        if (asset != null) {
          valueBophan = asset.boPhanQuanLy;
          picColtroller.text =
              asset.nguoisudung == '' ? ' ' : asset.nguoisudung;
          if (valueLocation != asset.location) {
            // await myDatabase.insertAssetDu(asset);
            assetDu = true;
          }
          focusPIC.requestFocus();
          picColtroller.selection = TextSelection(
            baseOffset: 0,
            extentOffset: picColtroller.text.length,
          );
        } else {
          picColtroller.text = ' ';
          valueBophan = listBoPhan[0];
        }
      });
      // if (asset != null) {
      //   // if (asset.noiScand == '') {
      //   //   asset.noiScand = valueBophan;
      //   //   asset.ngayscand = myCurrentDate;
      //   //   asset.scanned = 'scanned';
      //   //   await myDatabase.updateAsset(asset);

      //   //   asset.noiScand = '';
      //   //   asset.ngayscand = '';
      //   // }
      //   if (valueLocation != asset.location) {
      //     // await myDatabase.insertAssetDu(asset);
      //     setState(() {
      //       assetDu = true;
      //     });
      //   }
      // } else {}
    });

    focusScan.requestFocus();
    scanController.selection = TextSelection(
      baseOffset: 0,
      extentOffset: scanController.text.length,
    );

    // focusScan.requestFocus();
    // scanController.selection = TextSelection(
    //   baseOffset: 0,
    //   extentOffset: scanController.text.length,
    // );
  }
}
