import 'package:asset_inven/conponent/text_description.dart';
import 'package:asset_inven/database/database.dart';
import 'package:asset_inven/model/asset.dart';
import 'package:flutter/material.dart';

class ListResidualScreen extends StatelessWidget {
  static String routeName = '/listresidualscreen';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List resedual asset'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FutureBuilder<List<Asset>>(
          future: MyDatabase.instance.getListAssetbyBophanDu(),
          builder: (context, snapshot) {
            List<Asset> listdu = [];
            listdu = snapshot.data;
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            } else {
              return ListView.builder(
                itemCount: listdu.length,
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.only(bottom: 5),
                    decoration: BoxDecoration(
                      color: Colors.blue.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextDescription(
                          text1: 'Code ',
                          text2: listdu[index].maTaiSan,
                        ),
                        TextDescription(
                          text1: 'Name ',
                          text2: listdu[index].tenTaiSan,
                        ),
                        TextDescription(
                          text1: 'Location ',
                          text2: listdu[index].location,
                        ),
                        TextDescription(
                          text1: 'Section ',
                          text2: listdu[index].boPhanQuanLy,
                        ),
                        TextDescription(
                          text1: 'Section scanned ',
                          text2: listdu[index].noiScand,
                        ),
                        TextDescription(
                          text1: 'Date scanned ',
                          text2: listdu[index].ngayscand,
                        ),
                      ],
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}
