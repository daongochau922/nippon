import 'package:asset_inven/conponent/text_description.dart';
import 'package:asset_inven/database/database.dart';
import 'package:asset_inven/model/asset.dart';
import 'package:flutter/material.dart';
import 'package:asset_inven/conponent/cricle_dialog.dart';
import 'package:toast/toast.dart';

import '../util.dart';

class AddMissingAssetScreen extends StatefulWidget {
  static String routeName = '/addMissingAssetScreen';

  @override
  _AddMissingAssetScreenState createState() => _AddMissingAssetScreenState();
}

class _AddMissingAssetScreenState extends State<AddMissingAssetScreen> {
  TextEditingController nameController;
  TextEditingController motaController;
  TextEditingController loaiController;
  FocusNode nameFocus;
  FocusNode motaFocus;
  FocusNode loaiFocus;
  @override
  void initState() {
    nameController = new TextEditingController();
    motaController = new TextEditingController();
    loaiController = new TextEditingController();
    nameFocus = new FocusNode();
    motaFocus = new FocusNode();
    loaiFocus = new FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    motaController.dispose();
    loaiController.dispose();
    nameFocus.dispose();
    motaFocus.dispose();
    loaiFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final AddMssingAssetScreenArgument args =
        ModalRoute.of(context).settings.arguments;
    final String code = args.code;
    final String bophan = args.bophan;
    final String location = args.location;
    final String pic = args.pic;
    FocusScope.of(context).requestFocus(nameFocus);
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Missing Asset'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextDescription(
                text1: 'Code ',
                text2: code,
              ),
              SizedBox(height: 5),
              TextDescription(
                text1: 'Location ',
                text2: location,
              ),
              SizedBox(height: 5),
              TextDescription(
                text1: 'Section ',
                text2: bophan,
              ),
              SizedBox(height: 5),
              TextDescription(
                text1: 'Pic ',
                text2: pic,
              ),
              SizedBox(height: 10),
              TextField(
                focusNode: nameFocus,
                autofocus: true,
                controller: nameController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  hintText: 'Input Name',
                  labelText: 'Name',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  border: OutlineInputBorder(),
                ),
                onSubmitted: (value) => loaiFocus.requestFocus(),
              ),
              SizedBox(height: 20),
              TextField(
                focusNode: loaiFocus,
                controller: loaiController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  hintText: 'Input Type',
                  labelText: 'Type',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  border: OutlineInputBorder(),
                ),
                onSubmitted: (value) => motaFocus.requestFocus(),
              ),
              SizedBox(height: 20),
              TextField(
                focusNode: motaFocus,
                controller: motaController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  hintText: 'Input Description',
                  labelText: 'Description',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 10),
              Center(
                child: FlatButton(
                  color: Colors.blue.withOpacity(0.9),
                  onPressed: () async {
                    LoadingDialog.showLoadingDialog(context);
                    Asset temp = await MyDatabase.instance.getAssetthieu(code);
                    if (temp == null) {
                      await MyDatabase.instance.insertAssetThieu(Asset(
                        maTaiSan: code,
                        location: location,
                        boPhanQuanLy: bophan,
                        moTa: motaController.text,
                        loai: loaiController.text,
                        nguoisudung: pic,
                        noiScand: location,
                        ngayscand: myCurrentDate,
                        ghichu: '',
                        ngayNhap: '',
                        stt: '',
                        tenTaiSan: nameController.text,
                      ));
                      Toast.show('Insert compelte', context,
                          duration: Toast.LENGTH_LONG);
                    } else {
                      Toast.show('Code already add in missing list', context,
                          duration: Toast.LENGTH_LONG);
                    }

                    LoadingDialog.hideLoadingDialog(context);
                  },
                  child: Text('Register'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class AddMssingAssetScreenArgument {
  final String code;
  final String location;
  final String bophan;
  final String pic;

  AddMssingAssetScreenArgument({
    this.code,
    this.bophan,
    this.location,
    this.pic,
  });
}
