import 'dart:async';

import 'package:asset_inven/database/database.dart';

class BlocLoadBoPhan {
  StreamController<Future<List<String>>> blocloadcontroller =
      StreamController<Future<List<String>>>();

  Stream get loadBophanStream => blocloadcontroller.stream;

  void getData() {
    blocloadcontroller.sink.add(MyDatabase.instance.getListBoPhan());
  }

  dispose() {
    blocloadcontroller.close();
  }
}
