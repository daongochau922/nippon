import 'dart:async';
import 'package:asset_inven/database/database.dart';
import 'package:mailer/mailer.dart';

import '../util.dart';

class BlocLoadLocation {
  StreamController<Future<List<String>>> blocloadLOCcontroller =
      StreamController<Future<List<String>>>();

  Stream get loadLocationStream => blocloadLOCcontroller.stream;

  void getData() {
    blocloadLOCcontroller.sink.add(MyDatabase.instance.getListLocation());
  }

  dispose() {
    blocloadLOCcontroller.close();
  }

  static Future<SendReport> sendEmail(List<Attachment> listfilePath) async {
    final smtpServer = outlookmail('ngotluan@nittsu.com.vn', "Tlp281100");
    var message = Message()
      ..from = Address(
        "ngotluan@nittsu.com.vn",
      )
      ..recipients.add('daonhau@nittsu.com.vn')
      ..ccRecipients.add("nguyenhtdung@nittsu.com.vn")
      ..ccRecipients.add('sgnit@nittsu.com.vn')
      ..subject = "Kiem ke tai san Nippon"
      ..text = "Dear All\n No reply, thank you\n File attached below!!!\n"
      ..attachments = listfilePath;
    try {
      final sendReport = await send(message, smtpServer);
      return sendReport;
    } on MailerException catch (e) {
      return Future.error(e);
    }
  }
}
