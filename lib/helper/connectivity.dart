import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';

class ConnectivityNetwork {
  static final ConnectivityNetwork _singleton =
      new ConnectivityNetwork._internal();
  ConnectivityNetwork._internal();

  //This is what's used to retrieve the instance through the app
  static ConnectivityNetwork getInstance() => _singleton;

  //flutter_connectivity
  final Connectivity _connectivity = Connectivity();

  void initialize() {
    _connectivity.onConnectivityChanged.listen(_connectionChange);
    checkConnection();
  }

  void _connectionChange(ConnectivityResult result) {
    checkConnection();
  }

  bool hasConnection = false;

  Future<bool> checkConnection() async {
    bool previousConnection = hasConnection;

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        hasConnection = true;
      } else {
        hasConnection = false;
      }
    } on SocketException catch (_) {
      hasConnection = false;
    }

    //The connection status changed send out an update to all listeners
    if (previousConnection != hasConnection) {
      //retur connectionChangeController.add(hasConnection);
      return hasConnection;
    }

    return hasConnection;
  }
}
