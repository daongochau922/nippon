import 'package:asset_inven/screen/add_missing_asset_screen.dart';
import 'package:asset_inven/screen/confirm_screen.dart';
import 'package:asset_inven/screen/list_missing_asset.dart';
import 'package:asset_inven/screen/list_resedual_screen.dart';
import 'package:asset_inven/screen/menu.dart';
import 'package:flutter/material.dart';

final Map<String, WidgetBuilder> routes = {
  MenuScreen.routeName: (context) => MenuScreen(),
  ConfirmScreen.routeName: (context) => ConfirmScreen(),
  ListResidualScreen.routeName: (context) => ListResidualScreen(),
  AddMissingAssetScreen.routeName: (context) => AddMissingAssetScreen(),
  ListMissingAssetScreen.routeName: (context) => ListMissingAssetScreen(),
};
