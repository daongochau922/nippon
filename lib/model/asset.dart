import 'package:asset_inven/database/database.dart';

class Asset {
  //Hello hau sa
  int id;
  String stt;
  String location;
  String section;
  String boPhanQuanLy;
  String nguoisudung;
  String maTaiSan;
  String loai;
  String tenTaiSan;
  String moTa;
  String ngayNhap;
  String ghichu;
  String scanned;
  String notfound;
  String ngayscand;
  String noiScand;

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = <String, dynamic>{
      colId: id,
      colstt: stt,
      collocation: location,
      colsection: section,
      colboPhanQuanLy: boPhanQuanLy,
      colNguoiSudung: nguoisudung,
      colmaTaiSan: maTaiSan,
      colLoai: loai,
      coltenTaiSan: tenTaiSan,
      colmota: moTa,
      colngayNhap: ngayNhap,
      colghichu: ghichu,
      colScaned: scanned,
      colnotfound: notfound,
      colngayScan: ngayscand,
      colnoiScan: noiScand
    };
    return map;
  }

  Asset({
    this.stt,
    this.location,
    this.section,
    this.boPhanQuanLy,
    this.nguoisudung,
    this.maTaiSan,
    this.loai,
    this.tenTaiSan,
    this.moTa,
    this.ngayNhap,
    this.ghichu,
    this.scanned,
    this.notfound,
    this.ngayscand,
    this.noiScand,
  });
  Asset.fromMap(Map<String, dynamic> map) {
    id = map[colId];
    stt = map[colstt];
    location = map[collocation];
    section = map[colsection];
    boPhanQuanLy = map[colboPhanQuanLy];
    nguoisudung = map[colNguoiSudung];
    maTaiSan = map[colmaTaiSan];
    loai = map[colLoai];
    tenTaiSan = map[coltenTaiSan];
    moTa = map[colmota];
    ngayNhap = map[colngayNhap];
    ghichu = map[colghichu];
    scanned = map[colScaned];
    notfound = map[colnotfound];
    ngayscand = map[colngayScan];
    noiScand = map[colnoiScan];
  }
}
