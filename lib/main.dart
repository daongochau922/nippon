import 'package:asset_inven/helper/connectivity.dart';
import 'package:asset_inven/route.dart';
import 'package:asset_inven/screen/menu.dart';
import 'package:flutter/material.dart';

import 'database/database.dart';

void main() async {
  ConnectivityNetwork connectionStatus = ConnectivityNetwork.getInstance();
  WidgetsFlutterBinding.ensureInitialized();
  connectionStatus.initialize();
  MyDatabase instance = MyDatabase.instance;
  await instance.getdatabase;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: MenuScreen.routeName,
      routes: routes,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}
